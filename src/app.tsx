import { FC } from "react";
import { use100vh } from "react-div-100vh";
import { createBrowserHistory, ReactLocation, Router } from "react-location";
import { AuthScreen } from "screens/auth";
import styles from "styles/app.module.scss";
import { LearnScreen } from "screens/learn";

const history = createBrowserHistory();
const location = new ReactLocation({ history });

export const App: FC = () => {
  const vh = use100vh() || 0;

  return (
    <div style={{ height: vh }} className={styles.app}>
      <Router
        routes={[
          { path: "/", element: <AuthScreen /> },
          { path: "/learn", element: <LearnScreen /> },
        ]}
        location={location}
      />
    </div>
  );
};
