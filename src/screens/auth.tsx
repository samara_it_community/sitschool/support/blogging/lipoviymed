import styles from "styles/auth.module.scss";
import { Popover } from "react-tiny-popover";
import { motion } from "framer-motion";
import { useState } from "react";
import { useNavigate } from "react-location";

export const AuthScreen = () => {
  const [pop, setPop] = useState(false);
  const navigate = useNavigate();

  const onGoogleAuth = () => {
    navigate({ to: "/learn" });
  };

  return (
    <>
      <div className={styles.sideblock}>
        <div className={styles.content}>
          <h1>Тут что-то будет. Мб онбоард</h1>
        </div>
      </div>
      <div className={styles.mainzone}>
        <div className={styles.login}>
          <div className={styles.headings}>
            <div className={styles.text}>
              <h1>Рады видеть в itish</h1>
              <h2>Давайте представимся</h2>
            </div>
            <div style={{ flex: 1 }} />
            <Popover
              isOpen={pop}
              positions={["bottom"]}
              padding={10}
              content={<p className={styles.pop}>Привет!</p>}
            >
              <motion.img
                src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/apple/285/hand-with-fingers-splayed_1f590-fe0f.png"
                alt=""
                onMouseEnter={() => setPop(true)}
                onMouseLeave={() => setPop(false)}
                whileHover={{ rotateZ: [0, -40, 40, 0] }}
                transition={{ repeat: 1 }}
              />
            </Popover>
          </div>

          <div className={styles.socials}>
            <button onClick={onGoogleAuth} />
            <button />
            <button />
          </div>

          <p className={styles.divider}>или</p>

          <form action="auth">
            <input
              type="text"
              placeholder="Email"
              enterKeyHint="next"
              required
            />
            <input
              type="password"
              placeholder="Password"
              enterKeyHint="done"
              required
            />
            <div className={styles.info}>
              <div className={styles.checkbox}>
                <input id="remember" type="checkbox" />
                <label htmlFor="remember">Запомнить меня</label>
              </div>
              <div style={{ flex: 1 }} />
              <a href="/whoami">Забыли пароль?</a>
            </div>
          </form>
          <button type="submit" form="auth" />
        </div>
      </div>
    </>
  );
};
